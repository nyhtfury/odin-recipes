# Odin Recipes

## Description

These are some recipes I found. I did not try them. YMMV.

This step of Foundations requires understanding Git and making sure SSH is properly set up. In addition, I needed to understand unordered and ordered lists, and links. Lastly, I had to get a refresher on how to host GitLab Pages, though The Odin Project explicitly asks for use of GitHub rather than GitLab...

## Support

Refer to [The Odin Project](https://www.theodinproject.com/) for support. They maintain a Discord server that is incredibly helpful if you follow the guidelines they set.

## Contributing

This repository isn't intended for contributions, but to rather document progress through [The Odin Project](https://www.theodinproject.com/)

## License

See [LICENSE.md](LICENSE.md)
